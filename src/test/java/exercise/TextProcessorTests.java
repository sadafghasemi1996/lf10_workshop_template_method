package java.exercise;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TextProcessorTests {

    @Test
    public void testUpperCaseProcessor() {
        TextProcessor upperCaseProcessor = new UpperCaseProcessor();

        String input = "   Hello, World!   ";
        String expectedResult = "Formatted (Uppercase): HELLO, WORLD!";
        String actualResult = upperCaseProcessor.processText(input);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testLowerCaseProcessor() {
        TextProcessor lowerCaseProcessor = new LowerCaseProcessor();

        String input = "   Hello, World!   ";
        String expectedResult = "Formatted (Lowercase): hello, world!";
        String actualResult = lowerCaseProcessor.processText(input);

        assertEquals(expectedResult, actualResult);
    }
}

