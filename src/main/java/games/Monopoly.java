package games;

import java.util.Random;
import java.util.Scanner;

class Monopoly extends Game {

    private int currentPlayerPosition = 0;


    @Override
    protected void initialize() {
        System.out.println("Setting up the Monopoly board.");
    }

    @Override
    protected void startPlay() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Press Enter to roll the dice or type 'exit' to end the game.");
            String input = scanner.nextLine();

            if (input.equalsIgnoreCase("exit")) {
                System.out.println("Thanks for playing!");
                break;
            }

            int diceResult = rollDice();
            System.out.println("You rolled: " + diceResult);

            move(diceResult);

            if (calculateWinner()) {
                System.out.println("Congratulations! You're the winner!");
                break;
            }
        }    }

    @Override
    protected void endPlay() {
        System.out.println("Ending the Monopoly game.");
    }


    // Non-abstract methods specific to Monopoly

    // Simulating rolling a dice
    private int rollDice() {
        return new Random().nextInt(6) + 1;
    }

    // Moving the player on the board
    private void move(int steps) {
        currentPlayerPosition = (currentPlayerPosition + steps) % 40;
        System.out.println("Player moved " + steps + " steps. Current position: " + currentPlayerPosition);
    }

    // Simulating the logic for calculating the winner
    boolean calculateWinner() {
        return currentPlayerPosition >= 20; // Player wins if they cross or reach the 20th position
    }


}
