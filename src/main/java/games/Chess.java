package games;

class Chess extends Game {

    @Override
    protected void initialize() {
        System.out.println("Setting up the chess board.");
    }

    @Override
    protected void startPlay() {
        System.out.println("Starting the chess game.");
    }

    @Override
    protected void endPlay() {
        System.out.println("Ending the chess game.");
    }

    // Non-abstract method specific to Chess
    public void announceWinner() {
        System.out.println("Announcing the winner of the chess game.");
    }

    @Override
    public void playGame() {
        super.playGame(); // Call the template method from the base class
        announceWinner(); // Call the specific method for Chess
    }
}