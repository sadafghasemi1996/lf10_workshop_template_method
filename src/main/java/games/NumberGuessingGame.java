package games;
import java.util.Random;
import java.util.Scanner;

public class NumberGuessingGame extends Game{
    protected int targetNumber;
    protected int attempts;
    protected Scanner scanner;
    private final int MAX_ATTEMPTS = 5;
    @Override
    protected void initialize() {
        // picking a random number
        Random random = new Random();
        targetNumber = random.nextInt(50) + 1; // Generate a number between 1 and 100
        attempts = 1;
        System.out.println("Welcome to the Number Guessing Game!");
        System.out.println("I have selected a number between 1 and 50. Try to guess it.");
    }

    protected boolean isGameOver() {
        return attempts > MAX_ATTEMPTS;
    }

    protected int getUserGuess() {
        scanner = new Scanner(System.in);
        System.out.print("Attempt " + attempts + "/" + MAX_ATTEMPTS + ": Enter your guess: ");
        return scanner.nextInt();
    }

    protected boolean isCorrectGuess(int guess) {
        return guess == targetNumber;
    }


    protected void displayWinningMessage() {
        System.out.println("Congratulations! You guessed the number " + targetNumber + " correctly!");
    }

    protected void displayHint(int guess) {
        if (guess < targetNumber) {
            System.out.println("Try a higher number.");
        } else {
            System.out.println("Try a lower number.");
        }
    }

    protected void displayGameOver() {
        System.out.println("Game over. The correct number was " + targetNumber + ".");
    }


    @Override
    protected void startPlay() {
        while (!isGameOver()) {
            int guess = getUserGuess();
            if (isCorrectGuess(guess)) {
                displayWinningMessage();
                break;
            } else {
                displayHint(guess);
            }
            attempts++;
        }
    }

    @Override
    protected void endPlay() {
        if (isGameOver()) {
            displayGameOver();
        }
    }
}
