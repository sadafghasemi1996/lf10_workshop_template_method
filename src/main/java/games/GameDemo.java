package games;


public class GameDemo {

    public static void main(String[] args) {
        Game chessGame = new Chess();
        Monopoly monopolyGame = new Monopoly();

//        System.out.println("Playing a game of chess:");
//        chessGame.playGame();

//        System.out.println("\nPlaying a game of Monopoly:");
//        monopolyGame.playGame();
//        monopolyGame.calculateWinner(); // Accessing non-abstract method
//
        Game g = new NumberGuessingGame();
        g.playGame();
    }
}