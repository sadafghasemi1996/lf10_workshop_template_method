# Text Processing Framework Exercise

## Scenario: 
You are building a text processing framework that takes a text string, processes it, and returns a formatted result. The processing includes parsing the text, transforming it into a specific format, and finally formatting it for display.
## Instructions:

1. Create an abstract class **TextProcessor** with the following template method structure:

```
abstract class TextProcessor {
    public final String processText(String input) {
        String parsedText = parseText(input);
        String transformedText = transformText(parsedText);
        String formattedText = formatText(transformedText);
        return formattedText;
    }

    protected abstract String parseText(String input);
    protected abstract String transformText(String parsedText);
    protected abstract String formatText(String transformedText);
}

```
2. Create two concrete subclasses that extend **TextProcessor**: **UpperCaseProcessor** and **LowerCaseProcessor**.
   1. **UpperCaseProcessor** should implement logic to parse, transform to uppercase, and format the text.
   2. **LowerCaseProcessor** should implement logic to parse, transform to lowercase, and format the text.
3. In the **main** method of a class **TextProcessorDemo**, create instances of **UpperCaseProcessor** and **LowerCaseProcessor**, and demonstrate their usage by processing different input text.
- **(Optional)** Create the Class diagram for this exercise. 